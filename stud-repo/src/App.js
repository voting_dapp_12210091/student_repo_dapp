//Step 2: change the base code to use Component
import React, { Component } from 'react'
import Web3 from 'web3'
import './App.css';

// import SearchBar from './components/search'

// Step 3: import the ABI
import { STUDENTRECORD_ABI, STUDENTRECORD_ADDRESS } from './abi/config_studentrecord'
import StudentRecord from './components/StudentRecord'

//We replace function with a component for better management of code
class App extends Component {
  //componentDidMount called once on the client after the initial render
  //when the client receives data from the server and before the data is displayed.
  componentDidMount() {
    if (!window.ethereum)
      throw new Error("No crypto wallet found. Please install it.");
    window.ethereum.send("eth_requestAccounts");
    this.loadBlockchainData()
  }
  //create a web3 connection to using a provider, MetaMask to connect to
  //the ganache server and retrieve the first account
  async loadBlockchainData() {
    const web3 = new Web3(Web3.givenProvider || "HTTP://127.0.0.1:7546")
    const accounts = await web3.eth.getAccounts()
    this.setState({ account: accounts[0] })
    //Step 4: load all the lists from the blockchain
    const studentRecord = new web3.eth.Contract(STUDENTRECORD_ABI, STUDENTRECORD_ADDRESS)
    //Keep the lists in the current state
    this.setState({ studentRecord })
    //Get the number of records for all list in the blockchain
    const studentCount = await studentRecord.methods.studentsCount().call()
    //Store this value in the current state as well
    this.setState({ studentCount })
    //Use an iteration to extract each record info and store
    //them in an array
    this.state.students = []
    for (var i = 1; i <= studentCount; i++) {
      const student = await studentRecord.methods.students(i).call()
      this.setState({
        students: [...this.state.students, student]
      })
    }



  }

  state = { inputText: "" };

  onClick = (e) => {
    console.log(this.data)
    this.setState({ inputText: e.target.value })
  }

  //Initialise the variables stored in the state
  constructor(props) {

    super(props)
    this.state = {
      account: '',
      studentCount: 0,
      students: [],
      loading: true,
    }
    this.addStudent = this.addStudent.bind(this)
    this.markGraduated = this.markGraduated.bind(this)
  }
  addStudent(sid, name, course, email) {
    this.setState({ loading: true })
    this.state.studentRecord.methods.addStudent(sid, name, course, email)
      .send({ from: this.state.account })
      .once('receipt', (receipt) => {
        this.setState({ loading: false })
        this.loadBlockchainData()
      })
  }

  markGraduated(sid) {
    this.setState({ loading: true })
    this.state.studentRecord.methods.markGraduated(sid)
      .send({ from: this.state.account })
      .once('receipt', (receipt) => {
        this.setState({ loading: false })
        this.loadBlockchainData()
      })
  }

  // data = Array.from(this.students)
  //Display the first account and student detail
  render() {
    return (
      // <div className="container">
      //   <h1>Hello, World!</h1>
      //   <p>Your account: {this.state.account}</p>
      // </div>
      <div className="container">
        <h1>Student Record System</h1>

        <p>Your account: {this.state.account}</p>
{/* 
        <Search data = {this.state.students}/> */}

        <StudentRecord addStudent={this.addStudent} />
      
        <input placeholder="Enter Student name to search " onChange={this.onClick} className="form-control mt-5" />

        <div>
          {this.state.inputText}
        </div>

        {/* <SearchBar student_Data={this.state.students} mark_graduate = {this.markGraduated} /> */}

        <table id="studentList" className="table table-striped mt-5 rounded">
          <thead className='thead table-dark'>

            <tr>
              <th>SL.NO</th>
              <th>Enrollenment Number</th>
              <th>Name</th>
              <th>Course</th>
              <th>Email</th>
              <th>Gradute</th>
            </tr>
          </thead>
          {
            //This gets the each student from the studentList
            //and pass them into a function that display the
            //details of the student

            this.state.students.map((student, key) => {
              return (
                <tbody key={key}>
                  <tr>
                    <th scope="row">{student._id}.</th>
                    <td>{student.sid}</td>
                    <td>{student.name}</td>
                    <td>{student.course}</td>
                    <td>{student.email}</td>
                    <td><input
                      className="form-check-input"
                      type="checkbox"
                      name={student._id}
                      defaultChecked={student.graduated}
                      disabled={student.graduated}
                      ref={(input) => {
                        this.checkbox = input
                      }}
                      onClick={(event) => {
                        this.markGraduated(event.currentTarget.name)
                      }}
                    />
                      <label className="form-check-label" >Graduated</label></td>
                  </tr>
                </tbody>

              )
            }
            )
          }
        </table>
      </div >
    );
  }


}
export default App;


