
// // import { useState } from "react"

// function Search({ student_Data, mark_graduate }) {

//     console.log(student_Data)

//     return (
//         <div>
//             <input placeholder="Enter Post Title" id="outlined-basic"
//                 // onChange={inputHandler}
//                 variant="outlined"
//                 fullWidth
//                 label="Search" />
//             <table id="studentList" className="table table-striped mt-5 rounded">
//                 <thead className='thead table-dark'>

//                     <tr>
//                         <th>SL.NO</th>
//                         <th>Enrollenment Number</th>
//                         <th>Name</th>
//                         <th>Course</th>
//                         <th>Email</th>
//                         <th>Gradute</th>
//                     </tr>
//                 </thead>
//                 {
//                     //This gets the each student from the studentList
//                     //and pass them into a function that display the
//                     //details of the student

//                     student_Data.map((student, key) => {
//                         return (
//                             <tbody key={key}>
//                                 <tr>
//                                     <th scope="row">{student._id}.</th>
//                                     <td>{student.sid}</td>
//                                     <td>{student.name}</td>
//                                     <td>{student.course}</td>
//                                     <td>{student.email}</td>
//                                     <td><input
//                                         className="form-check-input"
//                                         type="checkbox"
//                                         name={student._id}
//                                         defaultChecked={student.graduated}
//                                         disabled={student.graduated}
//                                         ref={(input) => {
//                                             checkbox = input
//                                         }}
//                                         onClick={(event) => {
//                                             mark_graduate(event.currentTarget.name)
//                                         }}
//                                     />
//                                         <label className="form-check-label" >Graduated</label></td>
//                                 </tr>
//                             </tbody>

//                         )
//                     }
//                     )
//                 }
//             </table>
//         </div>
//     )
// }

// export default Search;
import { useState } from "react";

const getFilteredItems = (query, items) => {
    if (!query) {
        return items;
    }
    return items.filter((song) => song.name.includes(query));
};

export default function Search( {data}) {
    const [query, setQuery] = useState("");

    const { S_items } = data;
    const { items } = S_items;
    // items looks like this: [{name: 'name1'}, {name: 'name2'}]

    const filteredItems = getFilteredItems(query, items);

    return (
        <div className="App">
            <label>Search</label>
            <input type="text" onChange={(e) => setQuery(e.target.value)} />
            <ul>
                {filteredItems.map((value) => (
                    <h1 key={value.name}>{value.name}</h1>
                ))}
            </ul>
        </div>
    );
}
